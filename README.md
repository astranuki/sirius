# sirius 

![keyboard layout](sirius.svg)

~~a keyboard for dogs~~

a 3x5+4 unibody with morningstar stagger, offset thumbs, outer palm keys, and cute case options

*Sirius is the brightest star in the night sky. Its name is derived from the Greek word Σείριος, or Seirios, meaning lit. 'glowing' or 'scorching'.* -wikipedia (retr. 5/10/2023)

## todo
- ergogen pcb outline
- ergogen footprints
- kicad tracks